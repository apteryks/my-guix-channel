(define-module (my packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages texinfo)
  #:use-module ((gnu packages emacs-xyz) #:prefix gnu:))

(define-public emacs-geiser
  (let ((revision "1")
	(commit "846b23d70c47b2d7b48000f2f2455ffd48405c33"))
    (package
      (inherit gnu:emacs-geiser)
      (name "emacs-geiser")
      (version (git-version "0.10.0" revision commit))
      (source (origin
		(method git-fetch)
		(uri (git-reference
		      (url "https://gitlab.com/jaor/geiser.git")
		      (commit commit)))
		(file-name (git-file-name name version))
		(sha256
		 (base32
		  "0mxb18zr7ip57wb7r59s36hv46jhjxr69fx65kkmd56lksifygi1"))))
      (native-inputs `(("emacs" ,emacs-minimal)
		       ("autoconf" ,autoconf)
		       ("automake" ,automake)
		       ("texinfo" ,texinfo))))))
